<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|unique:products|max:255',
            'category_id' => 'required',
            'content' => 'required',
            'myfile' => 'required',
            'price' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'name.unique' => 'Tên đã được sử dụng',
            'category_id.required' => 'Danh mục không được để trống',
            'myfile.required' => 'Ảnh không được để trống',
            'price.required' => 'Giá không được để trống',
            'content.required' => 'Content không được để trống',
        ];
    }
}
