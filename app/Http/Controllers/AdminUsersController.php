<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminUsersController extends Controller
{
    private $user;
    private $role;
    public function __construct(User $user,Role $role){
        $this->user = $user;
        $this->role = $role;
    }
    public function index(){
        $users = $this->user->paginate(10);
        return view('admin.user.index',compact('users'));

    }
    public function create(){
        $role =$this->role->all();
        return view('admin.user.add',compact('role'));

    }
    public function store(Request $request){

        $user = $this->user->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        $user->roles()->attach($request->role_id);
        return redirect(route('users.index'));
    }
    public function edit($id){
        $user = $this->user->find($id);
        $role =$this->role->all();
        return view("admin.user.Edit",compact('role','user'));
    }
    public function update($id,Request $request){
        $this->user->find($id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        $user = $this->user->find($id);
        $user->roles()->sync($request->role_id);
        return redirect(route('users.index'));
    }
    public function delete($id){
        $user = $this->user->find($id)->delete();

        return redirect(route('users.index'));
    }
}
