<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\AddProductRequest;
use App\Products;
use Illuminate\Http\Request;
use Storage;
class ProductController extends Controller
{
    private $htmtSlelect;
    public function _construct(){
        $this->htmlSlelect = '';
    }
    public function categoryRecuaslve($id,$parentId){
        $data = Category::all();
        foreach ($data as $value){
            if($value['parent_id'] == $id  ){
                if(!empty($parentId) && $parentId == $value['id']){
                    $this->htmtSlelect.= "<option selected value='".$value['id']."'>" .$value['name'].'</option>';
                }else {
                    $this->htmtSlelect .= "<option value='" . $value['id'] . "'>" . $value['name'] . '</option>';
                }

                $this->categoryRecuaslve($value['id'],$parentId);
            }

        }
        return $this->htmtSlelect;
    }
    public function index()
    {
        $product = Products::paginate(5);
        return view('admin.product.index',compact('product'));
    }

    public function create(){
        $htmlOption = $this->categoryRecuaslve(0,'');
        return view('admin.product.add',compact('htmlOption'));
    }

    public function store(AddProductRequest $request)
    {
        if($request->hasFile('myfile')){
            $file = $request->file('myfile');
            $filename = $file->getClientOriginalName('myfile');
            $file->move('img',$filename);
            $img = 'img/'.$filename;

        }
        else{echo"chưa có file";}
        $data = [
            'name' => $request->name,
            'price' => $request->price,
            'content' => $request->contents,
            'user_id' => '1',
            'category_id' => $request->category_id,
            'feature_image_path'=> $img,
            'feature_image_name'=>$filename
        ];
        Products::create($data);
        return redirect(route('product.index'));

    }
    public function delete($id)
    {
        Products::find($id)->delete();
        return redirect(route('product.index'));
    }


}
