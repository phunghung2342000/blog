@extends('admin.layouts.master')

@section('title')
@endsection
@section('css')
    <link href="{{ asset('asset/admin/addProduct/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="col-md-6">
        <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Tên sản phẩm</label>
                <input type="text" value='{{ old('name') }}' class="form-control" placeholder="Nhập tên danh mục" name="name">
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label>Giá sản phẩm</label>
                <input type="text" value='{{ old('price') }}' class="form-control" placeholder="Nhập giá sản phẩm" name="price">
            </div>
            @error('price')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label>Ảnh đại diện</label>
                <input value='{{ old('myfile') }}' type="file" class="form-control-file"  name="myfile">
            </div>
            @error('myfile')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label>Nhập nội dung</label>
                <textarea class="form-control my-editor"  name="contents" value='{{ old('contents') }}'></textarea>
            </div>
            @error('content')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label >Chọn danh mục</label>
                <select class="form-control chon" name="category_id" multiple="multiple">
                    <option value="0">chọn</option>
                    {!! $htmlOption !!}
                </select>
            </div>
            @error('category_id')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
@section('js')
    <script src="{{ asset('asset/admin/addProduct/select2.min.js') }}"></script>
    <script src="{{ asset("//cdn.tinymce.com/4/tinymce.min.js") }}"></script>

    <script >
        $(function (){
            $(".chon").select2({
                placeholder: "Select a state",
                allowClear: true,
            })
        })
    </script>
@endsection

