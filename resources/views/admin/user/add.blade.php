@extends('admin.layouts.master')

@section('title')
@endsection
@section('css')
    <link href="{{ asset('asset/admin/addProduct/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="col-md-6">
        <form action="{{ route('users.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Tên</label>
                <input type="text" value='{{ old('name') }}' class="form-control" placeholder="Nhập tên " name="name">

            </div>

            <div class="form-group">
                <label>Email</label>
                <input type="email" value='{{ old('email') }}' class="form-control" placeholder="Nhập email" name="email">
            </div>

            <div class="form-group">
                <label>Password</label>
                <input type="text" class="form-control" placeholder="Nhập password"  name="password">
            </div>
            <div class="form-group">
                <label >Chọn vai trò</label>
                <select class="form-control chon" name="role_id[]" multiple>

                    @foreach($role as $key => $value)
                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach

                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
@section('js')
    <script src="{{ asset('asset/admin/addProduct/select2.min.js') }}"></script>
    <script src="{{ asset("//cdn.tinymce.com/4/tinymce.min.js") }}"></script>

    <script >
        $(function (){
            $(".chon").select2({
                placeholder: "Select a state",
                allowClear: true,
            })
        })
    </script>
@endsection


