@extends('admin.layouts.master')

@section('title')
    Users
@endsection

@section('content')


    <div class = "col-md-12">
        <a href="{{ route('users.create') }}" class="btn btn-success m-2">Add</a>
    </div>
    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">STT</th>
                <th scope="col">Tên</th>
                <th scope="col">Email</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $key => $value)
                <tr>

                    <th scope="row">{{$key+1}}</th>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->email }}</td>

                    <td>
                        <a href="{{ route('users.edit', ['id' => $value->id])}}" class="btn btn-dark">Edit</a>
                        <a href="{{ route('users.delete', ['id' => $value->id])}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
    <div class = "col-md-12"></div>
@endsection
