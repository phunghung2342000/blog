
@extends('admin.layouts.master')

@section('title')
    Danh mục sản phẩm
@endsection

@section('content')
    <div class = "col-md-12">
        <a href="{{ route('categories.create') }}" class="btn btn-success m-2">Add</a>
    </div>

    <div class="col-md-12">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">STT</th>
                <th scope="col">Tên danh mục</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($category as $key => $value)
                <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td>{{ $value->name }}</td>
                    <td>
                        <a href="{{ route('categories.edit', ['id' => $value->id])  }}" class="btn btn-dark">Edit</a>
                        <a href="{{ route('categories.delete', ['id' => $value->id]) }}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
    <div class = "col-md-12">{{ $category->links() }}</div>
@endsection
