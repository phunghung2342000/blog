<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});
Route::prefix('admin/categories')->group(function () {
    Route::get('/',[
        'as' =>'categories.index',
        'uses' => 'CategoryController@index',
    ]);
    Route::get('/create',[
        'as' =>'categories.create',
        'uses' => 'CategoryController@create',
    ]);
    Route::post('/store',[
        'as' =>'categories.store',
        'uses' => 'CategoryController@store',
    ]);
    Route::get('/edit/{id}',[
        'as' =>'categories.edit',
        'uses' => 'CategoryController@edit',
    ]);
    Route::get('/delete/{id}',[
        'as' =>'categories.delete',
        'uses' => 'CategoryController@delete',
    ]);
    Route::post('/update/{id}',[
        'as' =>'categories.update',
        'uses' => 'CategoryController@update',
    ]);

});
Route::prefix('admin/products')->group(function () {
    Route::get('/',[
        'as' =>'product.index',
        'uses' => 'ProductController@index',
    ]);
    Route::get('/add',[
        'as' =>'product.create',
        'uses' => 'ProductController@create',
    ]);
    Route::post('/store',[
        'as' =>'product.store',
        'uses' => 'ProductController@store',
    ]);
    Route::get('/delete/{id}',[
        'as' =>'product.delete',
        'uses' => 'ProductController@delete',
    ]);

});

Route::prefix('users')->group(function () {
    Route::get('/',[
        'as' =>'users.index',
        'uses' => 'AdminUsersController@index',
    ]);
    Route::get('/create',[
        'as' =>'users.create',
        'uses' => 'AdminUsersController@create',
    ]);
    Route::post('/store',[
        'as' =>'users.store',
        'uses' => 'AdminUsersController@store',
    ]);
    Route::get('/edit/{id}',[
        'as' =>'users.edit',
        'uses' => 'AdminUsersController@edit',
    ]);
    Route::get('/delete/{id}',[
        'as' =>'users.delete',
        'uses' => 'AdminUsersController@delete',
    ]);
    Route::post('/update/{id}',[
        'as' =>'users.update',
        'uses' => 'AdminUsersController@update',
    ]);
    Route::get('/delete/{id}',[
        'as' =>'users.delete',
        'uses' => 'AdminUsersController@delete',
    ]);

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'HomeController@admin')->name('admin');
Route::get('/post/list', 'PostController@index')->name('post.admin');
Route::get('post/show{id}', 'PostController@show')->name('post.show');

